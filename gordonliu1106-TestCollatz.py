#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "100 200\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 100)
        self.assertEqual(j, 200)

    def test_read_3(self):
        s = "201 10000\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 201)
        self.assertEqual(j, 10000)

    def test_read_4(self):
        s = "0 0\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 0)
        self.assertEqual(j, 0)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(1000, 5000)
        self.assertEqual(v, 238)

    # Corner Case: If the first number is greater than second, then collatz_eval should still work.
    def test_eval_6(self):
        v = collatz_eval(5000, 1000)
        self.assertEqual(v, 238)

    # Tests list of length 1.
    def test_eval_7(self):
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1)

    # Corner Case: If the two numbers create an invalid range, then collatz_eval should return 0.
    # Range is invalid because the range includes a number n < 1. Collatz conjecture is for natural numbers. 
    def test_eval_8(self):
        v = collatz_eval(0, 100)
        self.assertEqual(v, 0)
    
    def test_eval_9(self):
        v = collatz_eval(1001, 2500)
        self.assertEqual(v, 209)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    # Corner Case: If last parameter is 0, print should output "Invalid Input" for last value.
    def test_print_2(self):
        w = StringIO()
        collatz_print(w, -1, 100, 0)
        self.assertEqual(w.getvalue(), "-1 100 Invalid Input\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 200, 300, 128)
        self.assertEqual(w.getvalue(), "200 300 128\n")

    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 1000, 5000, 238)
        self.assertEqual(w.getvalue(), "1000 5000 238\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    # Corner Case: Empty string input. Should not write anything to given writer.
    def test_solve_2(self):
        r = StringIO("")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "")

    # Corner Case: If inputs are invalid, should write Invalid Input after the range for the corresponding line.
    def test_solve_4(self):
        r = StringIO("1 10\n100 200\n-1 1\n0 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n-1 1 Invalid Input\n0 1000 Invalid Input\n")

    # Failure Case: New correct ranges.
    def test_solve_5(self):
        r = StringIO("1000 5000\n200 300\n999900 999999\n1 1\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1000 5000 238\n200 300 128\n999900 999999 259\n1 1 1\n")

    def test_solve_6(self):
        r = StringIO("1000 5000\n \n200 300\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1000 5000 238\n200 300 128\n")


# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
