#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)
    
    def test_read_1(self):
        s = "200 1839\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 200)
        self.assertEqual(j, 1839)
    
    def test_read_2(self):
        s = "48274 33344\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 48274)
        self.assertEqual(j, 33344)
    
    def test_read_3(self):
        s = "987654 500999\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 987654)
        self.assertEqual(j, 500999)

    def test_read_4(self):
        s = "28899 499988\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 28899)
        self.assertEqual(j, 499988)
        
    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 3)
        self.assertEqual(v, 8)

    def test_eval_2(self):
        v = collatz_eval(100, 95)
        self.assertEqual(v, 119)

    def test_eval_3(self):
        v = collatz_eval(5629, 8573)
        self.assertEqual(v, 262)

    def test_eval_4(self):
        v = collatz_eval(800000, 837799)
        self.assertEqual(v, 525)

    def test_eval_5(self):
        v = collatz_eval(45803,80789)
        self.assertEqual(v, 351)

    # -----
    # print
    # -----
    def test_print(self):
        w = StringIO()
        collatz_print(w, 11, 99, 119)
        self.assertEqual(w.getvalue(), "11 99 119\n")
    
    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 600, 2345, 183)
        self.assertEqual(w.getvalue(), "600 2345 183\n")
    
    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 2222, 5555, 238)
        self.assertEqual(w.getvalue(), "2222 5555 238\n")
        
    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 987654, 988000, 354)
        self.assertEqual(w.getvalue(), "987654 988000 354\n")
        
    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 111111, 100000, 354)
        self.assertEqual(w.getvalue(), "111111 100000 354\n")
        
    def test_print_5(self):
        w = StringIO()
        collatz_print(w, 222, 777, 171)
        self.assertEqual(w.getvalue(), "222 777 171\n")
    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")
            
    def test_solve_2(self):
        r = StringIO("45 923\n18473 8248\n3852 4758\n77 7777\n2948 28499\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "45 923 179\n18473 8248 279\n3852 4758 215\n77 7777 262\n2948 28499 308\n")
            
    def test_solve_3(self):
        r = StringIO("67890 80000\n298 2748\n69 420\n7829 13987\n39999 42069\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "67890 80000 351\n298 2748 209\n69 420 144\n7829 13987 276\n39999 42069 288\n")
            
    def test_solve_4(self):
        r = StringIO("2048 37508\n4983 5948\n38457 29083\n38 788\n49743 98472\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "2048 37508 324\n4983 5948 236\n38457 29083 324\n38 788 171\n49743 98472 351\n")
            
    def test_solve_5(self):
        r = StringIO("586 2388\n20938 4718\n3 16\n69 58\n58483 20938\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "586 2388 183\n20938 4718 279\n3 16 20\n69 58 108\n58483 20938 340\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""

