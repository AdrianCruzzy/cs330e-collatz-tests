#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "100 200\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 100)
        self.assertEqual(j, 200)

    def test_read_3(self):
        s = "1 1\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 1)

    def test_read_4(self):
        s = "10 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 10)
        self.assertEqual(j, 10)

    def test_read_5(self):
        s = "5 1\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 5)
        self.assertEqual(j, 1)

    def test_read_6(self):
        s = "10 1\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 10)
        self.assertEqual(j, 1)

    def test_read_7(self):
        s = "5 1\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 5)
        self.assertEqual(j, 1)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1)

    def test_eval_6(self):
        v = collatz_eval(10, 10)
        self.assertEqual(v, 7)

    def test_eval_7(self):
        v = collatz_eval(10, 1)
        self.assertEqual(v, 20)

    def test_eval_8(self):
        v = collatz_eval(5, 1)
        self.assertEqual(v, 8)

    def test_eval_9(self):
        v = collatz_eval(1, 100)
        self.assertEqual(v, 119)

    def test_eval_10(self):
        v = collatz_eval(100000, 700000)
        self.assertEqual(v, 509)

    def test_eval_11(self):
        v = collatz_eval(700000, 100000)
        self.assertEqual(v, 509)

    def test_eval_12(self):
        v = collatz_eval(1039, 10039)
        self.assertEqual(v, 262)

    def test_eval_13(self):
        v = collatz_eval(10039, 1039)
        self.assertEqual(v, 262)

    def test_eval_14(self):
        v = collatz_eval(999999, 999999)
        self.assertEqual(v, 259)

    def test_eval_15(self):
        v = collatz_eval(1, 999999)
        self.assertEqual(v, 525)

    def test_eval_16(self):
        v = collatz_eval(999999, 1)
        self.assertEqual(v, 525)

    def test_eval_17(self):
        v = collatz_eval(777234, 17)
        self.assertEqual(v, 509)

    def test_eval_18(self):
        v = collatz_eval(17, 777234)
        self.assertEqual(v, 509)

    def test_eval_19(self):
        v = collatz_eval(1001, 1002)
        self.assertEqual(v, 143)

    def test_eval_20(self):
        v = collatz_eval(1002, 1001)
        self.assertEqual(v, 143)

    def test_eval_21(self):
        v = collatz_eval(1002, 1003)
        self.assertEqual(v, 112)

    def test_eval_22(self):
        v = collatz_eval(1003, 1002)
        self.assertEqual(v, 112)

    def test_eval_23(self):
        v = collatz_eval(1, 2463)
        self.assertEqual(v, 209)

    def test_eval_24(self):
        v = collatz_eval(2463, 1)
        self.assertEqual(v, 209)

    def test_eval_25(self):
        v = collatz_eval(3711, 6000)
        self.assertEqual(v, 238)

    def test_eval_26(self):
        v = collatz_eval(6000, 3711)
        self.assertEqual(v, 238)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 10, 10, 7)
        self.assertEqual(w.getvalue(), "10 10 7\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 5, 1, 8)
        self.assertEqual(w.getvalue(), "5 1 8\n")

    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 100, 200, 125)
        self.assertEqual(w.getvalue(), "100 200 125\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2(self):
        r = StringIO("10 10\n100 200\n5 1\n200 300\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "10 10 7\n100 200 125\n5 1 8\n200 300 128\n")

    def test_solve_3(self):
        r = StringIO("1 1\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 1 1\n")

    def test_solve_4(self):
        r = StringIO("10 1\n201 210\n1 10\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "10 1 20\n201 210 89\n1 10 20\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
